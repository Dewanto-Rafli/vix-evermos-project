package configs

import (
	"gitlab.com/Dewanto-Rafli/vix-evermos-project/exceptions"

	"github.com/gofiber/fiber/v2"
)

func NewFiberConfig() fiber.Config {
	return fiber.Config{
		ErrorHandler: exceptions.ErrorHandler,
	}
}
